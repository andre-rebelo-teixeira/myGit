#ifndef HEADERS
#define HEADERS

#define MAX 50

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QDebug>
#include <iostream>
#include <QString>
#include <QHeaderView>
#include <list>
#include <iomanip>
#include <algorithm>

using namespace std;

class Vehicle
{
    private:
        /* data */
    protected:
        int wheels;
        int serial;
        double price;
        QString brand;
        virtual int get_wheels_count()
        {
            return wheels;
        }
    public:
        virtual void set_serial(int s)
        {
            serial = s;
        }
        virtual void set_price(double p)
        {
            price = p;
        }
        virtual void set_brand(QString b)
        {
            brand = b;
        }
        virtual void set_Wheels(int w)
        {
            wheels  = w;
        }
        virtual int get_serial()
        {
            return serial;
        }
        virtual int get_price()
        {
            return price;
        }
        virtual QString get_brand()
        {
            return brand;
        }
};
class Car : public Vehicle
{
    private:
        //no private data
    public:
        int get_wheels_count() override
        {
            return wheels;
        }
        void set_serial(int s) override
        {
            serial = s;
        }
        void set_price(double  p) override
        {
            price = p;
        }
        void set_Wheels(int w) override
        {
            wheels  = w;
        }
        void set_brand(QString b) override
        {
            brand = b;
        }
        int get_serial() override
        {
            return serial;
        }
        int get_price() override
        {
            return price;
        }
        QString get_brand() override
        {
            return brand;
        }

};

class Motorcycle : public Vehicle
{
    private:
        int wheels = 2;
    public:
        int get_wheels_count() override
        {
            return wheels;
        }
        void set_serial(int s) override
        {
            serial = s;
        }
        void set_price(double p) override
        {
            price = p;
        }
        void set_brand(QString b) override
        {
            brand = b;
        }
        int get_serial() override
        {
            return serial;
        }
        int get_price() override
        {
            return price;
        }
        QString get_brand() override
        {
            return brand;
        }

};

class CarDealer
{
    private:
        list<Motorcycle*> MOTO_List;
        list<Car*> CAR_List;
        QTableWidget *table;
    public:
        CarDealer(QTableWidget *table1);
        ~CarDealer();

        // Tabela das elementos (2 dimensões (x e y))
        QTableWidgetItem* get (int row, int column);

        // Não sabia como comparar se existia e o que mandar quado fosse nulo então separeia as coisas

        // Adicionar
        void add_car(Car Carro);
        void add_moto(Motorcycle Moto);

        // Remover
        void remove_car(int row);
        void remove_moto(int row);

        // Editar
        void edit_car(int row, Car Novo);
        void edit_moto (int row, Motorcycle novo);

};

#endif
